# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Customer, FloorPlan, Order

# Register your models here.
admin.site.register(Customer)
admin.site.register(FloorPlan)
admin.site.register(Order)
