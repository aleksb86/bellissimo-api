# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from uuid import uuid4
from api.common.common import rand_string


# Create your models here.
class Customer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=255, null=True, unique=True)
    created_datetime = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return "id:{} - {}".format(self.id, self.name)


class FloorPlan(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    common_name = models.CharField(max_length=255, null=True, unique=True)
    link_to_floor_plan = models.CharField(max_length=1024, null=True)
    link_to_seat = models.CharField(max_length=1024, null=True)
    floor_plan_height = models.IntegerField(null=False, default=0)
    floor_plan_width = models.IntegerField(null=False, default=0)
    created_datetime = models.DateTimeField(auto_now_add=True, null=True)
    customer = models.ForeignKey(Customer, related_name='floor_plans', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "id:{} by customer: {}".format(self.id, self.common_name, self.customer)


class Seat(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    position_x = models.fields.IntegerField(null=False, default=0)
    position_y = models.fields.IntegerField(null=False, default=0)
    description = models.fields.CharField(max_length=500, null=True)
    floor_plan = models.ForeignKey(FloorPlan, related_name='seats', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "id:{} x:{}, y:{}".format(self.id, str(self.position_x), str(self.position_y))


class Order(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    order_num = models.CharField(max_length=6, default=rand_string(6), unique=True)
    created_datetime = models.DateTimeField(auto_now_add=True, null=True)
    event_datetime = models.DateTimeField(null=True)
    phone_number = models.fields.CharField(max_length=15, null=True)
    canceled = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)
    seat = models.ForeignKey(Seat, related_name='orders', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "id: {} order_num: {}".format(self.id, self.order_num)
