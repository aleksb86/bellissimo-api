from rest_framework import serializers
from django.contrib.auth.models import User
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from .models import Customer, FloorPlan, Order, Seat
# from rest_framework.validators import UniqueValidator


class SeatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seat
        fields = '__all__'

    def validate(self, data):
        if (r'position_x' not in data) or (r'position_y' not in data):
            raise ValidationError("Fields 'position_x' and 'position_y' "
                                "are required!")

        return data


class FloorPlanSerializer(serializers.ModelSerializer):

    seats = SeatSerializer(many=True, read_only=True)

    class Meta:
        model = FloorPlan
        fields = (
            'id',
            'common_name',
            'link_to_floor_plan',
            'link_to_seat',
            'floor_plan_height',
            'floor_plan_width',
            'created_datetime',
            'customer',
            'seats',
        )

    def validate_common_name(self, value):
        if not value:
            raise serializers.ValidationError("Field 'common_name' is required!")
        elif FloorPlan.objects.filter(common_name=value.upper()).exists():
            raise serializers.ValidationError("Floor plan with common_name '{}' "
                                            "already exists!".format(value))
        return value.upper()

    def validate(self, data):
        url_validator = URLValidator()
        if r'link_to_floor_plan' in data:
            url_validator(data.get(r'link_to_floor_plan'))
        if r'link_to_seat' in data:
            url_validator(data.get(r'link_to_seat'))

        return data


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

    def validate_phone_number(self, value):
        if not value:
            raise ValidationError("Field 'phone_number' is required!")
        return value


class CustomerSerializer(serializers.ModelSerializer):

    floor_plans = FloorPlanSerializer(many=True, read_only=True)

    class Meta:
        model = Customer
        fields = ('id', 'name', 'floor_plans')

    def validate_name(self, value):
        if not value:
            raise serializers.ValidationError("Field 'name' is required!")
        elif Customer.objects.filter(name=value.upper()).exists():
            raise serializers.ValidationError("Customer with name '{}' "
                                            "already exists!".format(value))
        return value.upper()


class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """
    token = serializers.CharField(max_length=255)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email")
