from django.urls import path
from api.common.optional_slash_router import OptionalSlashRouter
from .views import (CustomersViewSet, SeatsViewSet,
                    FloorPlansViewSet, OrdersViewSet,
                    LoginView, RegisterUsers)

router = OptionalSlashRouter()
router.register(r'customers', CustomersViewSet)
router.register(r'floor_plans', FloorPlansViewSet)
router.register(r'orders', OrdersViewSet)
router.register(r'seats', SeatsViewSet)
urlpatterns = [
    path('auth/login/', LoginView.as_view(), name="auth-login"),
    path('auth/register/', RegisterUsers.as_view(), name="auth-register")
]
urlpatterns += router.urls
