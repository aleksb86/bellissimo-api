from rest_framework.routers import SimpleRouter


class OptionalSlashRouter(SimpleRouter):
    """ Class for router which insensitive to
        the presence or absence of trailing slash.
    """
    def __init__(self):
        self.trailing_slash = '/?'
        super(SimpleRouter, self).__init__()
