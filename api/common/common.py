"""
It is module for common useful methods.
"""

# -*- coding: utf-8 -*-

import string
import random




def rand_string(size, chars=None):
    """ Method for generating pseudorandom string with determined length """
    return ''.join(random.choice(chars or string.ascii_uppercase + string.digits) for _ in range(size))
